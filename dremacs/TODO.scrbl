#lang scribble/manual


@title{TODO}
@itemlist[#:style 'ordered
@item{Figure out why the menu% has default values in them @bold{DONE}} 
@item{Figure out why the save method does not work @bold{DONE} }
@item{Get Insert Mode to allow typing when not in Insert Mode }
@item{Figure out how to open a file and set text}
@item{Create a shortcut for beginning and end of file use M-< and M->}
@item{Create a init file that does customization}
@item{Embed Racket Repl into Dremacs}
@item{Add vim insert mode}
@item{Figure out how to do strike throughs and sub items in scribble}
@item{Configure c:a to not select everything in the text window, just move to beginning of line @bold{Done}}
@item{Figure out what a Snip is}
@item{Figure out how to configure text shortcuts @bold{DONE}}
@item{}]


@section{Useful docs and source code}


@itemlist[#:style 'ordered
@item{Check out this page on @hyperlink["https://docs.racket-lang.org/gui/Editor_Functions.html"]{editor functions}}
@item{Check out this page on @hyperlink["https://docs.racket-lang.org/gui/keymap_.html"]{Key Maps}}
@item{@hyperlink["https://github.com/racket/gui/blob/0afd5ffda6ad714e1b88b49de6486f8777599297/gui-lib/mred/private/wxme/text.rkt"]{Source cdoe for add-text-keymap-function!}}
@item{check out this function for rendering html @hyperlink["https://docs.racket-lang.org/browser/index.html#%28def._%28%28lib._browser%2Fhtmltext..rkt%29._render-html-to-text%29%29"]{render html}}
@item{}]
