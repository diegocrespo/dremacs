#lang scribble/manual



@title{HTML Parsing}


@italic{DOM HTML/DOM} - In-memory representation of html.
@italic{MIME type} - Internet media type as defined by Multipurpose Internet Mail Extensions (MIME)
@itemlist[#:style 'unordered
@item{@italic{MIME type} - ASCII string (non-empty)}
@item{@italic{MIME subtype} - ASCII String (non-empty)}
@item{@italic{MIME parameter} - an ordered map whose keys and values are ASCII strings. initally empty}
] 



@section{Encoding Sniffing Algorithm}
html is decoding from a string of bytes. The @italics{encoding sniffing algorithm} is used to determine the character encoding.

Given a character encoding, the bytes in the input byte stream must be converted to characters for the tokenizers input stream.

If no encoding is necessary, e.g. because the parser is operating on a Unicode stream and doesn't have to use a character encoding at all, then the confidence is irrelevant.

Before the tokenization stage, the input stream must be preprocessed by normalizing newlines. Thus, newlines in HTML DOMs are represented by U+000A LF characters, and there are never any U+000D CR characters in the input to the tokenization stage
@italic{Next Input Character} - First Char in the input stream that has not yet been consumed. Initially it's the first character in the input.
@italic{Current Input Character} - last character that has been consumed
@italic{Insertion Point} - Position (just before a char or just before teh end of the input stream)where content is inserted using document.write() is actually inserted.

@italic{Lexer} - Creates tokens from the input
@italic{Parser} - Creates parse tree by analyzing document structure.

@section{Parser State}

@italic{Insertion Mode} - state variable that controls the primary operation of the tree construction stage. The states are...

@itemlist[#:style 'unordered
@item{initial}
@item{before html}
@item{before head}
@item{in head}
@item{in head noscript}
@item{after head}
@item{in body}
@item{text}
@item{in table}
@item{in table text}
@item{in caption}
@item{in column group}
@item{in table body}
@item{in row}
@item{in cell}
@item{in select}
@item{in select in table}
@item{in template}
@item{after body}
@item{in frameset}
@item{after frameset}
@item{after after body}
@item{after after frameset}] 

@bold{in head} @bold{in body} @bold{in table} @bold{in select}  are special modes. Other modes defer to these modes

When the insertion mode is switched to "text" or "in table text", the @bold{original insertion mode}  is also set. This is the insertion mode to which the tree construction stage will return.

@italic{list of active elements} - Empty initialy. Used to handle mis-nested formatting elements.








@bold{Questions}
@itemlist[#:style 'unordered
@item{What does "A leading Byte Order Mark (BOM) causes the character encoding argument to be ignored and will itself be skipped." mean?}
@item{Why are CESU-8, UTF-7, BOCU-1, SCSU, EBCDIC, and UTF-32 prohibited encodings? }
@item{What are template elements?}
@item{How do I implement tokens ?}
@item{How do I implement the dom ?}
@item{@italic{What does this section is non-normative mean? in the sections of the html specification?}}] 
