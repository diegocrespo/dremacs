# Dremacs

**Dr** **E**liza **Macs** is a text editor intended to meet these goals...
1. Accessible - Leverage Rackets #lang features to allow customization in languages with _syntaxes_ you are familiar with
2. Extensible - Because leaving your text editor is a loss in productivity
3. Self Documenting - Because being feature rich means nothing if people don't know what you can do
4. Friendly to Non-Programmers - Not everyone can code, but lots of people have meaningful work to do that requires editing text. Use Racket's #lang scribble in the **W**hat **Y**ou **S**ee **I**s **W**hat **Y**ou **G**et (WYSIWYG) to quickly zip 
5. Emacs or Vim style keybindings - Because people have preferences even if some of them are wrong ;)


The goal of this project is not to replace Emacs, Vim, DrRacket, or other text editors. It's to make it easier to edit text when writing papers, using LaTeX, writing notes etc. 